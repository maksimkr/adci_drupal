<?php

/* modules/ubercart/uc_store/templates/uc-price.html.twig */
class __TwigTemplate_0dd624cb972b602cb9d7adc455714e9f97836b78a6b5f9e1dc6a509a9d1fbd5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_98823b0025a6a90e67ab2cb3b8b63a25b91aa18052f104a3ff1f17edbe65a9fe = $this->env->getExtension("native_profiler");
        $__internal_98823b0025a6a90e67ab2cb3b8b63a25b91aa18052f104a3ff1f17edbe65a9fe->enter($__internal_98823b0025a6a90e67ab2cb3b8b63a25b91aa18052f104a3ff1f17edbe65a9fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "modules/ubercart/uc_store/templates/uc-price.html.twig"));

        $tags = array("spaceless" => 14, "if" => 16);
        $filters = array("join" => 17);
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('spaceless', 'if'),
                array('join'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 14
        ob_start();
        // line 15
        echo "<span class=\"uc-price\">";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["price"]) ? $context["price"] : null), "html", null, true));
        echo "</span>
";
        // line 16
        if ( !twig_test_empty((isset($context["suffixes"]) ? $context["suffixes"] : null))) {
            // line 17
            echo "  <span class=\"price-suffixes\">";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, twig_join_filter((isset($context["suffixes"]) ? $context["suffixes"] : null), " "), "html", null, true));
            echo "</span>
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_98823b0025a6a90e67ab2cb3b8b63a25b91aa18052f104a3ff1f17edbe65a9fe->leave($__internal_98823b0025a6a90e67ab2cb3b8b63a25b91aa18052f104a3ff1f17edbe65a9fe_prof);

    }

    public function getTemplateName()
    {
        return "modules/ubercart/uc_store/templates/uc-price.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 17,  53 => 16,  48 => 15,  46 => 14,);
    }
}
/* {#*/
/* /***/
/*  * @file*/
/*  * Default theme implementation to display a price.*/
/*  **/
/*  * Displays a price in the standard format and with consistent markup.*/
/*  **/
/*  * Available variables:*/
/*  * - price: A formatted price.*/
/*  * - suffixes: An array of suffixes to be attached to this price.*/
/*  **/
/*  * @ingroup themeable*/
/* #}*/
/* {% spaceless %}*/
/* <span class="uc-price">{{ price }}</span>*/
/* {% if suffixes is not empty %}*/
/*   <span class="price-suffixes">{{ suffixes|join(' ') }}</span>*/
/* {% endif %}*/
/* {% endspaceless %}*/
/* */
